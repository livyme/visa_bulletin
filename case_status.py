#!/usr/bin/env python3
__author__ = 'livy'

import json
import logging

from slack import full_path
from slack import send_message

import requests
from bs4 import BeautifulSoup

logger = logging.getLogger('VISA STATUS')


def is_changed(result):
    filename = 'result.json'
    try:
        with open(full_path(filename)) as f:
            old_result = json.load(f)
    except FileNotFoundError:
        old_result = {'status': 'first_run'}
        logger.debug('{} does not exists. Previous result empty.'.format(filename))

    if result['status'] == old_result['status']:
        logger.debug('status not changed.')
        return False
    else:
        with open(full_path(filename), 'w') as f:
            json.dump(result, f)
            logger.debug('Saved new result to file {}'.format(filename))
        return True


class CaseError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class CaseStatus(object):
    def __init__(self):
        self.url = 'https://egov.uscis.gov/casestatus/mycasestatus.do'

    def get_status_html(self, number):
        post_data = {'initCaseSearch': 'CHECK+STATUS',
                     'appReceiptNum': number}
        headers = {
            'Connection': 'keep-alive',
            'Cache-Control': 'max-age=0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36',
            'Accept-Encoding': 'deflate',
            'Accept-Language': 'en-US,en;q=0.8,zh-CN;q=0.6,zh;q=0.4',
            'Cookie': 'mt=ci%3D-1_0; v=0; thw=glo; cookie2=185d3ecc29f02c76eaf11f59deb8645f; t=111d8645fb99b29ac183cd49cb38fa62; _tb_token_=e68583134e45b; JSESSIONID=BEB89A39AA336DEAB65FEB2E316C3AA6; uc1=cookie14=UoW0EwOvgEqrrw%3D%3D; isg=D8FAC15B2F6B92FE8CF63A815277B60D; l=AaaZygkQphommvNMaEnc0yaaJhUmmqYa'
        }
        response = requests.post(self.url, data=post_data, headers=headers)
        return response.text

    def single_case_result(self, case_number):
        content = self.get_status_html(case_number)
        soup = BeautifulSoup(content, 'lxml')
        status = soup.find('div', {'class': 'rows text-center'})
        if status:
            result = {'status': status.h1.string,
                      'details': status.p.get_text()}
        else:
            raise CaseError('No such case')
        return result

    def multiple_case_results(self, case_numbers):
        result = {}
        for number in case_numbers:
            result[number] = self.single_case_result(number)
        return result


def search_one_detect_change_update_slack(case_number):
    case = CaseStatus()
    result = case.single_case_result(case_number)

    if is_changed(result):
        result_string = 'NEW STATUS FOUND!!!! {status} - {details}'.format(**result)
    else:
        result_string = 'Status not changed. Current status: {status}.'.format(**result)
    logger.info(result_string)
    send_message(result_string)


if __name__ == '__main__':
    logging.basicConfig(level=logging.ERROR)

    search_one_detect_change_update_slack('LIN1790209102')

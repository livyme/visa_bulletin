#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import requests

from bs4 import BeautifulSoup


class VisaBulletin(object):
    """
    Visa Bulletin class
    Get the most up-to-date visa bulletin as published in US government website.

    Class attributes:
        host:  USCIS Visa Bulletin website http host address
        url:  USCIS Visa Bulletin website http full url
        eb_table_css_class:  CSS class of the Employment Based table
        recent_bulletins_css_id:  CSS ID of the recent bulletins div
        eb_categories:  different Employment based category name

    Instance attributes:
        dates: dictionary, return recent two month's EB priority dates.
            Only populated after parse() is called.
        _result_two_urls: dictionary, {'Month year': 'URL', 'June 2015': 'URL'}

    Methods:
        parse: get the current and future visa bulletin priority dates
    """
    host = 'http://travel.state.gov/'
    url = host + 'content/visas/english/law-and-policy/bulletin.html'
    eb_table_css_class = 'visabulletinemploymenttable parbase employment_table_data'
    recent_bulletins_css_id = 'recent_bulletins'
    # TODO: dynamically read eb_categories
    eb_categories = ['Employment Based',
                     'EB-1',
                     'EB-2',
                     'EB-3',
                     'EB-Other workers',
                     'EB-4',
                     'Certain Religious Workers']

    def __init__(self):
        self.dates = {}
        self._recent_two_urls = {}

    def _parse_content_get_links(self):
        content = requests.get(self.url).text
        soup = BeautifulSoup(content, 'lxml')
        for link in soup.find('ul', id=self.recent_bulletins_css_id).find_all('a'):
            month_year = ' '.join(link.get('title').split()[-2:])
            self._recent_two_urls[month_year] = self.host + link.get('href')

    def _parse_one_content_get_dates(self, url):
        content = requests.get(url).text
        soup = BeautifulSoup(content, 'lxml')
        # TODO: Last row does not appear parsed right...
        table_rows = soup.find('div', class_=self.eb_table_css_class).find('table').find_all('tr')
        result = {}

        # Country name is the column header.
        # Skipping 0 - not country name there
        for country in range(1, 6):
            for i, row in enumerate(table_rows):
                cell = row.find_all('td')
                if i == 0:  # row 0 is country name
                    country_name = cell[country].string
                    result[country_name] = {}
                elif i == 7:
                    # TODO: wft? Last row not parsing correctly
                    pass
                    # print cell
                else:
                    # change status to "current" if "C"
                    if cell[country].string == 'C':
                        status = "Current"
                    else:
                        status = cell[country].string

                    # print ('Reading from {} \n'
                    #        'Country index {}, {}, {}').format(cell, country, country_name, status)
                    result[country_name][self.eb_categories[i]] = status
        return result

    def parse(self):
        self._parse_content_get_links()
        if self._recent_two_urls:
            for link in self._recent_two_urls:
                self.dates[link] = {}
                result = self._parse_one_content_get_dates(self._recent_two_urls[link])
                self.dates[link] = result


if __name__ == '__main__':
    import json

    vb = VisaBulletin()
    vb.parse()
    print(json.dumps(vb.dates, indent=4, sort_keys=True))

###################
#  Example output:
# {
#     "June 2015": {
#         "All Chargeability Areas Except Those Listed": {
#             "Certain Religious Workers": "Current",
#             "EB-1": "Current",
#             "EB-2": "Current",
#             "EB-3": "15FEB15",
#             "EB-4": "Current",
#             "EB-Other workers": "15FEB15"
#         },
#         "CHINA - mainland born": {
#             "Certain Religious Workers": "Current",
#             "EB-1": "Current",
#             "EB-2": "01JUN13",
#             "EB-3": "01SEP11",
#             "EB-4": "Current",
#             "EB-Other workers": "01JAN06"
#         },
#         "INDIA": {
#             "Certain Religious Workers": "Current",
#             "EB-1": "Current",
#             "EB-2": "01OCT08",
#             "EB-3": "22JAN04",
#             "EB-4": "Current",
#             "EB-Other workers": "22JAN04"
#         },
#         "MEXICO": {
#             "Certain Religious Workers": "Current",
#             "EB-1": "Current",
#             "EB-2": "Current",
#             "EB-3": "15FEB15",
#             "EB-4": "Current",
#             "EB-Other workers": "15FEB15"
#         },
#         "PHILIPPINES": {
#             "Certain Religious Workers": "Current",
#             "EB-1": "Current",
#             "EB-2": "Current",
#             "EB-3": "01JAN05",
#             "EB-4": "Current",
#             "EB-Other workers": "01JAN05"
#         }
#     },
#     "May 2015": {
#         "All Chargeability Areas Except Those Listed": {
#             "Certain Religious Workers": "Current",
#             "EB-1": "Current",
#             "EB-2": "Current",
#             "EB-3": "01JAN15",
#             "EB-4": "Current",
#             "EB-Other workers": "01JAN15"
#         },
#         "CHINA - mainland born": {
#             "Certain Religious Workers": "Current",
#             "EB-1": "Current",
#             "EB-2": "01JUN12",
#             "EB-3": "01MAY11",
#             "EB-4": "Current",
#             "EB-Other workers": "15NOV05"
#         },
#         "INDIA": {
#             "Certain Religious Workers": "Current",
#             "EB-1": "Current",
#             "EB-2": "15APR08",
#             "EB-3": "15JAN04",
#             "EB-4": "Current",
#             "EB-Other workers": "15JAN04"
#         },
#         "MEXICO": {
#             "Certain Religious Workers": "Current",
#             "EB-1": "Current",
#             "EB-2": "Current",
#             "EB-3": "01JAN15",
#             "EB-4": "Current",
#             "EB-Other workers": "01JAN15"
#         },
#         "PHILIPPINES": {
#             "Certain Religious Workers": "Current",
#             "EB-1": "Current",
#             "EB-2": "Current",
#             "EB-3": "01JUL07",
#             "EB-4": "Current",
#             "EB-Other workers": "01JUL07"
#         }
#     }
# }

#!/usr/bin/env python3
__author__ = 'livy'

import json
import logging
import sys
from os import sep

import requests

logger = logging.getLogger('VISA STATUS')


def full_path(file_name):
    return '{}{}{}'.format(sys.path[0], sep, file_name)


def get_cred():
    filename = 'settings.json'
    try:
        with open(full_path(filename)) as f:
            cred = json.load(f)
    except FileNotFoundError:
        logger.critical('{} not found.  Please create this settings file.  Reference the sample file.'.format(filename))
        sys.exit(1)
    return cred


def send_message(text):
    cred = get_cred()
    data = {'text': text}
    headers = {'Content-Type': 'application/json'}
    response = requests.post(url=cred['slack_token'], data=json.dumps(data), headers=headers)
    if response.status_code == 200:
        return response.text
    else:
        raise Exception('Error {} when trying to send slack message.'.format(response.status_code))

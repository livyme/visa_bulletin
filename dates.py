#!/usr/bin/env python3
__author__ = 'livy'

import json
import logging
import logging.config

import requests
from bs4 import BeautifulSoup
from bs4.element import Tag

from slack import full_path
from slack import send_message


def get_logging_config_from_json(file_name):
    try:
        with open(file_name) as f:
            logging_dict = json.load(f)
        logging.config.dictConfig(logging_dict)
    except json.decoder.JSONDecodeError:
        logging.error('Can\'t decode {}. Make sure it is in correct JSON format'.format(file_name))
    except FileNotFoundError:
        logging.error('Can\'t open {}. Make sure it exists.\n  '
                      'Create one by referencing the logging.json.sample file.'.format(file_name))


def get_full_page(url):
    response = requests.get(url)
    if response.status_code == 200:
        result = response.text
    else:
        raise Exception('Get {} from {}'.format(response.status_code, url))
    return result


class USCISBulletinDates(object):
    url = 'https://www.uscis.gov/visabulletininfo'
    chart_A_string = 'Final Action Dates'
    chart_B_string = 'Dates for filing'
    local_result_file_name = 'dates_result.json'

    def __init__(self, logger=None):
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger(__name__)
        self.result = {'current': {}, 'next': {}}

    def find_current_dates(self):
        content = get_full_page(self.url)
        soup = BeautifulSoup(content, 'lxml')
        status = soup.find('div', {'id': 'main-body'})

        for tag in status.children:
            if isinstance(tag, Tag):
                if 'Current' in tag.text:
                    self.result['current'] = {
                        'file_table': self.find_date_string(tag.next_sibling.next_sibling.text),
                        'month': tag.next_sibling.next_sibling.next_sibling.text
                    }
                if 'Next' in tag.text:
                    self.result['next'] = {
                        'file_table': self.find_date_string(tag.next_sibling.next_sibling.text),
                        'month': tag.next_sibling.next_sibling.next_sibling.text
                    }

    def find_date_string(self, text):
        if self.chart_A_string in text:
            result = 'A'
        elif self.chart_B_string in text:
            result = 'B'
        else:
            self.logger.exception('Unknown date in {}.'.format(text))
            raise Exception('unknown date: {}'.format(text))
        return result

    def is_changed(self):
        try:
            with open(self.local_result_file_name) as f:
                old_dates = json.load(f)
        except json.decoder.JSONDecodeError:
            self.logger.warning('Cannot decode JSON from {}. This file will be overwritten.'.format(file_name))
            old_dates = None
        except FileNotFoundError:
            self.logger.warning('Result file {} does not exist.  Will be created on first run'.format(file_name))
            old_dates = None

        if old_dates == self.result:
            return False
        else:
            with open(self.local_result_file_name, 'w') as f:
                json.dump(self.result, f, indent=2)
            return True


def main():
    get_logging_config_from_json(full_path('logging.json'))
    logger = logging.getLogger('VISA dates')
    bulletin_date = USCISBulletinDates(logger)
    bulletin_date.find_current_dates()
    meaningful_message = ''
    for key in bulletin_date.result:
        meaningful_message += 'Use chart {file_table} for {month}.\n '.format(**bulletin_date.result[key])
    if bulletin_date.is_changed():
        logger.info('New date used: {}'.format(json.dumps(bulletin_date.result)))
        # additional processing, can be sent to slack or email
        result = send_message('Change detected on USCIS Visa Bulletin: \n{}'.format(meaningful_message))
        logger.info('Sent message via Slack.  Slack API returned: {}'.format(result))

    else:
        logger.info('No change')
        # result = send_message('No change to USCIS Visa Bulletin. \n{}'.format(meaningful_message))
        # logger.info('Sent message via Slack.  Slack API returned: {}'.format(result))


if __name__ == '__main__':
    main()
